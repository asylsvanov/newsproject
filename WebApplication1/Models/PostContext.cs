﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Models
{
    public class PostContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<User> Users { get; set; }

        public PostContext(DbContextOptions<PostContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            User user = new User { Id = 1, Email = "asylsvanov@mail.ru", Password = "asyl" };
            modelBuilder.Entity<User>().HasData(new User[] { user });
            base.OnModelCreating(modelBuilder);
        }
    }
}
