﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Language { get; set; }
        public string ShortContent { get; set; }
        public string FullContent { get; set; }
        public byte[] PreviewPhoto { get; set; }
        public byte[] MainPhoto { get; set; }
    }
}
// Politics Business ShowBusiness  Sociesty
