﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        PostContext db;
        public HomeController(PostContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            return View(db.Posts.ToList());
        }

     
        [Authorize]
        public IActionResult Add()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            return Content("не аутентифицирован");
        }

        [Authorize]
        public IActionResult Edit(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.PostId = id;
                return View(db.Posts.ToList());
            }
            return Content("не аутентифицирован");
        }
        
        [HttpPost, ActionName("Edit")]
        public async Task<ActionResult> Edit(Post post, List<IFormFile> PreviewPhoto, List<IFormFile> MainPhoto)
        {
            foreach (var item in PreviewPhoto)
            {
                if (item.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await item.CopyToAsync(stream);
                        post.PreviewPhoto = stream.ToArray();
                    }
                }
            }

            foreach (var item in MainPhoto)
            {
                if (item.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await item.CopyToAsync(stream);
                        post.MainPhoto = stream.ToArray();
                    }
                }
            }
            db.Posts.Update(post);
            db.SaveChanges();
            return RedirectPermanent("~/Home/List");
        }
        

        [Authorize]
        public IActionResult Delete(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.PostId = id;
                return View(db.Posts.ToList());
            }
            return Content("не аутентифицирован");
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult Delete(Post post)
        {
            db.Posts.Remove(post);
            // сохраняем в бд все изменения
            db.SaveChanges();
            return RedirectPermanent("~/Home/List");
        }
        
        [HttpGet]
        public IActionResult Record(int id)
        {
            ViewBag.PostId = id;
            return View(db.Posts.ToList());
        }

        [HttpPost]
        public async Task<ActionResult> Add(Post post, List<IFormFile> PreviewPhoto, List<IFormFile> MainPhoto)
        {
            foreach (var item in PreviewPhoto)
            {
                if (item.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await item.CopyToAsync(stream);
                        post.PreviewPhoto = stream.ToArray();
                    }
                }
            }

            foreach (var item in MainPhoto)
            {
                if (item.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await item.CopyToAsync(stream);
                        post.MainPhoto = stream.ToArray();
                    }
                }
            }
            db.Posts.Add(post);
            db.SaveChanges();
            return RedirectPermanent("~/Home/List");
        }

        [Authorize]
        public IActionResult List()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View(db.Posts.ToList());
            }
            return Content("не аутентифицирован");
        }


        public IActionResult Business()
        {
            return View(db.Posts.ToList());
        }

        public IActionResult ShowBusiness()
        {
            return View(db.Posts.ToList());
        }

        public IActionResult Politics()
        {
            return View(db.Posts.ToList());
        }

           public IActionResult Society()
        {
            return View(db.Posts.ToList());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
